import UserController from './controler/User_ctrl'
import AuthController from './controler/Auth_ctrl'
import BucketController from './controler/Bucket_ctrl'
import BlobController from './controler/Blob_ctrl'

export const Routes = [

  /**
   * User
   */
  {
    method: 'post',
    route: '/auth/register',
    controller: AuthController,
    action: 'register',
  },
  {
    method: 'get',
    route: '/users',
    controller: UserController,
    action: 'findAll',
  },
  {
    method: 'get',
    route: '/user/:uuid',
    controller: UserController,
    action: 'findOne',
  },
  {
    method: 'patch',
    route: '/user/update/:uuid',
    controller: UserController,
    action: 'patch'
  },
  {
    method: 'delete',
    route: '/user/delete/:uuid',
    controller: UserController,
    action: 'delete'
  },

  /**
   * Bucket
   */
  {
    method: 'post',
    route: '/bucket/create',
    controller: BucketController,
    action: 'save',
  },
  {
    method: 'get',
    route: '/buckets',
    controller: BucketController,
    action: 'findAll',
  },
  {
    method: 'get',
    route: '/bucket/:uuid',
    controller: BucketController,
    action: 'findOne',
  },
  {
    method: 'patch',
    route: '/bucket/update/:uuid',
    controller: BucketController,
    action: 'patch'
  },
  {
    method: 'delete',
    route: '/bucket/delete/:uuid',
    controller: BucketController,
    action: 'delete'
  },

  /**
   * Blob
   */
  {
    method: 'post',
    route: '/blob/create',
    controller: BlobController,
    action: 'save',
  },
  {
    method: 'get',
    route: '/blobs',
    controller: BlobController,
    action: 'findAll',
  },
  {
    method: 'get',
    route: '/blob/:uuid',
    controller: BlobController,
    action: 'findOne',
  },
  {
    method: 'patch',
    route: '/blob/update/:uuid',
    controller: BlobController,
    action: 'patch'
  },
  {
    method: 'delete',
    route: '/blob/delete/:uuid',
    controller: BlobController,
    action: 'delete'
  },

]

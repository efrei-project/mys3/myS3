import uuidv4 from 'uuid/v4'
import { getRepository } from 'typeorm'
import { Request, Response } from 'express'
import { User } from '../entities/User'

export default class AuthController {
  private userRepository = getRepository(User)

  async register(request: Request, response: Response): string {
    return this.userRepository.save({ ...request.body, uuid: uuidv4() })
  }
}

import { getRepository } from 'typeorm'
import { Request, Response } from 'express'
import { User } from '../entities/User'

export default class UserController {
  private userRepository = getRepository(User)

  async findAll(request: Request, response: Response): string {
    const users: [User] = await this.userRepository.find()

    if (users.length <= 0) return { error: 'No users found !' }

    return { users }
  }

  async findOne(request: Request, response: Response): string {
    const user: User = await this.userRepository.findOne({
      where: {
        uuid: request.params.uuid,
      },
    })

    if (!user) return { error: 'No user found !' }

    return { user }
  }

  async save(request: Request, response: Response): string {
    return this.userRepository.save(request.body)
  }

  async remove(request: Request, response: Response): string {
    const userToRemove = await this.userRepository.findOne(request.params.uuid)
    await this.userRepository.remove(userToRemove)
  }
}

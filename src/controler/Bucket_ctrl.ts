import { getRepository } from 'typeorm'
import { Request } from 'express'
import { Bucket } from '../entities/Bucket'

export default class BucketController {
  private bucketRepository = getRepository(Bucket)

  /**
   * Create a bucket
   * params: request.body with name, patch size, bucket
   * return: Promise of Bucket*/
  async save(request: Request): Promise<Bucket> {
    const { name, user } = request.body
    return this.bucketRepository.save({ name, user })
  }

  /**
   * Read a / all bucket(s)
   * params:
   * return: Promise of Bucket*/
  async findAll(): Promise<Bucket[]> {
    return await this.bucketRepository.find()
  }

  async findOne(request: Request): Promise<Bucket | undefined> {
    return await this.bucketRepository.findOne({
      where: {
        uuid: request.params.uuid,
      },
    })
  }

  /**
   * Update a bucket
   * params: request.body with name, patch size, bucket
   * return: Promise of Bucket*/
  async patch(request: Request): Promise<Bucket | undefined>{
    const { name, user, id } = request.body
    let bucket: Bucket | undefined =  await this.bucketRepository.findOne({
      where: {
        uuid: id,
      },
    })
    if(bucket){
      bucket.name = name
      bucket.user = user
      this.bucketRepository.save(bucket)
    }
    return bucket
  }

  /**
   * Delete a bucket
   * params: request.body with name, patch size, bucket
   * return: Promise of Bucket*/
  async remove(request: Request): Promise<Bucket | undefined> {
    const bucketToRemove: Bucket | undefined = await this.bucketRepository.findOne(request.params.uuid)
    bucketToRemove && await this.bucketRepository.remove(bucketToRemove)
    return bucketToRemove
  }


}

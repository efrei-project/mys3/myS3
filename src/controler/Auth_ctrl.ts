import { getRepository } from 'typeorm'
import { Request, Response } from 'express'
import { User } from '../entities/User'

export default class AuthController {
  private userRepository = getRepository(User)

  async register(request: Request): Promise<User> {
    const { nickname, password, email } = request.body
    return this.userRepository.save({ nickname, password, email })
  }
}

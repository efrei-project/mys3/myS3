import { getRepository } from 'typeorm'
import { Request } from 'express'
import { User } from '../entities/User'

export default class UserController {
  private userRepository = getRepository(User)

  /**
   * Create a blob
   * params: request.body with name, patch size, bucket
   * return: Promise of Blob*/
  async save(request: Request): Promise<User> {
    const { nickname, password, email } = request.body
    return this.userRepository.save({ nickname, password, email })
  }

  /**
   * Read a / all blob(s)
   * params:
   * return: Promise of Blob*/
  async findAll(): Promise<User[]> {
    return await this.userRepository.find()
  }

  async findOne(request: Request): Promise<User | undefined> {
    return await this.userRepository.findOne({
      where: {
        uuid: request.params.uuid,
      },
    })
  }

  /**
   * Update a blob
   * params: request.body with na
   me, patch size, bucket
   * return: Promise of Blob*/
  async patch(request: Request): Promise<User | undefined>{
    const { email, nickname, password, uuid } = request.body
    let user: User | undefined =  await this.userRepository.findOne({
      where: {
        uuid: uuid,
      },
    })
    if(user){
      user.nickname = nickname
      user.email = email
      user.password = password
      this.userRepository.save(user)
    }
    return user
  }

  /**
   * Delete a blob
   * params: request.body with name, patch size, bucket
   * return: Promise of Blob*/
  async delete(request: Request): Promise<User | undefined> {
    const userToRemove: User | undefined = await this.userRepository.findOne(request.params.uuid)
    userToRemove && await this.userRepository.remove(userToRemove)
    return userToRemove
  }
}

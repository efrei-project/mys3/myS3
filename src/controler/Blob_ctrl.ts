import { getRepository } from 'typeorm'
import { Request } from 'express'
import { Blob } from '../entities/Blob'

export default class BlobController {
  private blobRepository = getRepository(Blob)

  /**
   * Create a blob
   * params: request.body with name, patch size, bucket
   * return: Promise of Blob*/
  async save(request: Request): Promise<Blob> {
    const { name, path, size, bucket } = request.body
    return this.blobRepository.save({ name, path, size, bucket })
  }

  /**
   * Read a / all blob(s)
   * params:
   * return: Promise of Blob*/
  async findAll(): Promise<Blob[]> {
    return await this.blobRepository.find()
  }

  async findOne(request: Request): Promise<Blob | undefined> {
    return await this.blobRepository.findOne({
      where: {
        uuid: request.params.uuid,
      },
    })
  }

  /**
   * Update a blob
   * params: request.body with name, patch size, bucket
   * return: Promise of Blob*/
  async patch(request: Request): Promise<Blob | undefined>{
    const { name, path, size, bucket, id } = request.body
    let blob: Blob | undefined =  await this.blobRepository.findOne({
      where: {
        uuid: id,
      },
    })
    if(blob){
      blob.name = name
      blob.path = path
      blob.size = size
      blob.bucket = bucket
      this.blobRepository.save(blob)
    }
    return blob
  }

  /**
   * Delete a blob
   * params: request.body with name, patch size, bucket
   * return: Promise of Blob*/
  async remove(request: Request): Promise<Blob | undefined> {
    const blobToRemove: Blob | undefined = await this.blobRepository.findOne(request.params.uuid)
    blobToRemove && await this.blobRepository.remove(blobToRemove)
    return blobToRemove
  }


}

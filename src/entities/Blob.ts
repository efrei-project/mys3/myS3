import { Column, Entity, PrimaryGeneratedColumn } from 'typeorm'

@Entity()
export class Blob {
  @PrimaryGeneratedColumn()
  id: ObjectId

  @Column('text')
  name: string

  @Column('text')
  path: string

  @Column('text')
  size: string

  //TODO change type and do the relation
  @Column('text')
  bucket: string

}

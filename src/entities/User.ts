import { Column, Entity, PrimaryGeneratedColumn } from 'typeorm'

@Entity()
export class User {
  @PrimaryGeneratedColumn()
  id: ObjectId

  @Column('text')
  nickname: string

  @Column('text')
  email: string

  @Column('text')
  password: string
}

//import 'reflect-metadata'
import express from 'express'
import bodyParser from 'body-parser'
import { createConnection } from 'typeorm'
import { Express, Request, Response } from 'express'
import { Routes } from './routes'

const PORT: number = parseInt(process?.env?.PORT ? process?.env?.PORT : '8080')

createConnection().then(async connection => {
  const app: Express = express()
  app.use(bodyParser.json())

  Routes.forEach(route => {
    app[route.method](route.route, async (req: Request, res: Response) => {
      try {
        const result = await new route.controller()[route.action](req, res)

        res.json(result)
      } catch (error) {
        console.log(error)
        res.json({ error: error.message })
      }
    })
  })

  app.listen(8080)
  console.log(`server started at http://localhost:${PORT}`)
})

import server from '../src/server'
import request from 'supertest'

afterAll( () => {
  server.close()
})

describe('Testing API Routes', () => {
  test('[Route] Get > /api', async () => {
    const response = await request(server).get('/api')

    expect(response.statusCode).toEqual(200)
    expect(response.body).toHaveProperty('name')
    expect(response.body).toHaveProperty('version')
  })

  test('[Route] Get > /user', async () => {
    const response = await request(server).get('/api/users')

    expect(response.statusCode).toEqual(200)
  })

  test('[Route] Get > /user/:id', async () => {
    const response = await request(server).get('/api/users/1')

    expect(response.statusCode).toEqual(200)
  })

  test('[Route] Put > /user/:uuid', async () => {
    const response = await request(server).put('/api/users/1')

    expect(response.statusCode).toEqual(200)
  })

  test('[Route] Delete > /user/:uuid', async () => {
    const response = await request(server).delete('/api/users/1')

    expect(response.statusCode).toEqual(200)
  })
})
